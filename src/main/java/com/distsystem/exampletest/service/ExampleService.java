package com.distsystem.exampletest.service;


import org.springframework.stereotype.Service;

@Service
public class ExampleService {


    public String getAString() {
        return "something";
    }


    public int multiplication(int x, int y) {

        return x * y;
    }
}
