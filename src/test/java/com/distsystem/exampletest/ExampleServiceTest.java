package com.distsystem.exampletest;




import com.distsystem.exampletest.service.ExampleService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)

public class ExampleServiceTest {


    @Autowired ExampleService exampleService;


    @Test
    public void checkIfMultiplyIsCorrect() {

        int resultOfMultiply =  exampleService.multiplication(3,4);
        Assert.assertTrue(resultOfMultiply== 12);
    }


}
